Fiscal Automation 2017
===
A quick start for adding new program sites to the Fiscal Service 2017 project.

1. Modifies menus and links in the header and footer includes. 
2. Adds baseline directories to fiscal-service-2017/public.  
3. Adds page-name.html and page-name.css files for specified pages.
4. Adds a baseline scaffold to each page-name.html page.
_(Page title, page specific CSS files, program specific header and footer includes)_ 
