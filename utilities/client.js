// Bash

var execSync = require( 'child_process' ).execSync;

module.exports = class bash {
  
  constructor( path ) {
    this.path = path; 
  }
  
  makeDir( dirName ) {
    return execSync(
      `mkdir -p ${this.path}/${dirName}`, 
      { stdio: [ null, null, null ], env: undefined }
    );
  }
  
  touch( file ) {
    return execSync(
      `touch ${this.path}/${file}`, 
      { stdio: [ null, null, null ], env: undefined }    
    );
  }
  
  cat( string, path ) {
    return execSync(
      `cat <<EOF > ${this.path}/${path} ${string}`,
      { stdio: [ null, null, null ], env: undefined }
    );
  }
}