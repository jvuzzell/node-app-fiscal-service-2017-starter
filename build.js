const data   = require( './config.json' );
const bash   = require('./utilities/client.js');
const logger = require('./utilities/logger.js');
const programHeader = require( './templates/headers/v1.js' );
const programFooter = require( './templates/footers/v1.js' );
const pageScaffold  = require( './templates/page_scaffold/v1.js' ); 

const prgAcronym = data.names.acronym; 
const prgName    = data.names.full; 

const client = new bash( data.public_project );
const header = new programHeader( data.header.menu, prgAcronym, prgName );
const footer = new programFooter( data.footer, prgAcronym, prgName );
const page   = new pageScaffold( prgAcronym, prgName );

var headerString = ""; 
var footerString = ""; 
var pageString   = "";

logger.info( 'NPM', `Running - Build.js` );


/// Step 1 - Create necessary directories and files
var includes     = `includes/${prgAcronym}`; 
var includeFiles = [ `${includes}/header.include`, `${includes}/footer.include` ];
var directories  = [ `${prgAcronym}`, `files/${prgAcronym}`, `includes/${prgAcronym}`, `styles/${prgAcronym}`, `images/${prgAcronym}` ];

for(let i = 0; i < directories.length; i++) {
  try {
    client.makeDir( `${directories[i]}` ); 
    logger.info( 'Build', `Added directory: ${data.public_project}/${directories[i]}` );
    if( directories[i] == includes ) {
      for(let n = 0; n < includeFiles.length; n++) {
        try {
          client.touch( includeFiles[n] );
          logger.info( 'Build', `Added file: ${data.public_project}/${includeFiles[n]}` );
        } catch( error ) {
          logger.error( 'Build ( line: 33 )', 'Failed to make include files', error.message  );
        }
      }
    }
  } catch( error ) {
    logger.error( 'Build ( line: 28 )', 'Unable to create directory', error.message );
  }  
}


/// Step 2 - Build header.include
headerString = header.build();

try {
  client.cat( headerString, `includes/${prgAcronym}/header.include` );
  logger.info( 'Build', `Modified file: ${data.public_project}/includes/${prgAcronym}/header.include` );
} catch( error ) {
  logger.error( 'Build ( line: 50 )', 'Failed to write header.include', error.message );
}


/// Step 3 - Build footer.include
footerString = footer.build();

try {
  client.cat( footerString, `includes/${prgAcronym}/footer.include` );
  logger.info( 'Build', `Modified file: ${data.public_project}/includes/${prgAcronym}/footer.include` );
} catch( error ) {
  logger.error( 'Build ( line: 61 )', 'Failed to write footer.include', error.message );
}


/// Step 4 - Build .html and .css files for specified pages within program
for( item in data.header.menu ) {
  
  if(item == "home")
    fileName = "index";
  else 
    fileName = item; 
    
  pageString = page.build( fileName, data.header.menu[item].display );
  
  try {
    client.touch( `${prgAcronym}/${fileName}.html` );
    logger.info( 'Build', `Added file: ${data.public_project}/${prgAcronym}/${fileName}.html` );
  } catch ( error ) {
    logger.error( 'Build ( line: 79 )', `Failed to create ${fileName}.html`,  error.message ); 
  }
  
  try {
    client.touch( `styles/${prgAcronym}/${fileName}.css` );
    logger.info( 'Build', `Added file: ${data.public_project}/styles/${prgAcronym}/${fileName}.css` );
  } catch ( error ) {
    logger.error( 'Build ( line: 84 )', `Failed to create ${fileName}.css`,  error.message );
  }   
   
  try {
    client.cat( pageString, `${prgAcronym}/${fileName}.html` );
    logger.info( 'Build', `Modified file: ${data.public_project}/${prgAcronym}/${fileName}.html` );
  } catch( error ) {
    logger.error( 'Build ( line: 92 )', `Failed to write ${fileName}.html`, error.message );
  }
}
