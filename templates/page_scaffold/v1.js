// Page Scaffold
// Note: Preserve awkward tabs and spacing for HTML file output 

module.exports = class scaffold { 
  
  constructor( acronym, name ) {
    this.prgAcronym = acronym; 
    this.prgName = name;
  }
  
  build( fileName, pageName ) {
    let pageStr = `
<!--#set var='page_title' value='${this.prgName} - ${pageName}' -->
<!--#set var='subsite_section' value='home' -->

<!--#set var='page_styles' value='<link rel="stylesheet" type="text/css" href="/styles/${this.prgAcronym}/${fileName}.css">' -->

<!--#include virtual='/includes/${this.prgAcronym}/header.include' -->


<!--#include virtual='/includes/${this.prgAcronym}/footer.include' -->`; 
            
    return pageStr;
  }
}