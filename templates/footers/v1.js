// Footer - 
// Note: Preserve awkward tabs and spacing for HTML file output 

module.exports = class programFooter {
  
  constructor( data, acronym, name ){
    this.data = data;
    this.prgAcronym = acronym;
    this.prgName =  name;
  }
 
  defineContacts( data ) {
    let columnStr = "";  
    let name, 
        email, 
        phone, 
        hours; 
            
      columnStr += `
      <div class="column contact">`;
          
      for(let item in data){
        for(let subItem in data[item]){
          let subData = data[item][subItem];
          switch ( subItem ) {
            case "name" : 
              name = `<h2>${subData}</h2>`;
              break;
            case "email" :
              email = `<a class="icon email" href="mailto:${subData}">${subData}</a>`;
              break;
            case "phone" :
              phone = `<a class="icon phone" href="${subData}">${subData}</a>`;
              break;
            case "hours" :
              hours = `<p class="hours">${subData}</p>`;
              break;
              }
            }
            
        columnStr += `
        <div>
          ${name}
          <ul class="unstyled">
            <li>${email}</li>
            <li>${phone}</li>
            <li>${hours}</li>
          </ul>
        </div>`;
          }
          
      columnStr += `
      </div>`;
    
    return columnStr;
  }
  
  defineLinks( data_1, data_2 ) {
    let columnStr = "";
        
      columnStr += `
      <div class="column links">
        <nav class="two-column">`;
          
        // sub column 1
        columnStr += `
          <div class="column">
            <ul class="unstyled">`;
            
            for(let item in data_1){
              let link = data_1[item];
              columnStr += `
              <li><a href="${link.url}">${link.display}</a></li>`;
            }
            
            columnStr += `
            </ul>
          </div>`;
            
        // sub column 2
        columnStr += `
          <div class="column">
            <ul class="unstyled">`;
            
            for(let item in data_2){
              let link = data_2[item];
              columnStr += `
              <li><a href="${link.url}">${link.display}</a></li>`;
            }
            
            columnStr += `
            </ul>
          </div>`;
  
          columnStr += `
        </nav>
      </div>`;// column end 

    return columnStr;     
  }
  
  build() {
    let footerStr = "";
    
    footerStr += `
</main>

<footer id="footer-${this.prgAcronym}" class="footer-subsite">
  <div class="lock">
    <div>© 2017</div>
    <a class="logotype-subsite" href="/${this.prgAcronym}">${this.prgName}</a>
    
    <div class="two-column">`;

footerStr += this.defineContacts( this.data.column_1 );
footerStr += this.defineLinks( this.data.column_2, this.data.column_3 );

footerStr += ` 
    </div>
    
  </div>
</footer>

<!--#include virtual='/includes/footer-subsite.include' -->`;
    
    return footerStr ;
  }
}