// Header
// Note: Preserve awkward tabs and spacing for HTML file output 

module.exports = class programHeader {
  
  constructor( data, acronym, name ){
    this.data = data;
    this.prgAcronym = acronym;
    this.prgName =  name;
  }
  
  defineMenu() {
    var menuStr = "";
    
    for( let item in this.data) {
      let slug = item;
      let props = this.data[item]; 
      let name = props.display;
      
        if( props.dropdown ) {
          menuStr += `
        <li class="menu">
          <button <!--#if expr='v("subsite_section") = "${slug}"' -->data-current-section<!--#endif --> aria-haspopup="true" aria-expanded="false">${name}</button>
          <ul class="unstyled">`;
          
          for( let subItem in this.data ) {
            let subSlug = subItem; 
            let subprops = this.data[subItem];
            let subName = subprops.display;
            
            if( subprops.parent == slug ) {
              menuStr += `
            <li>
              <a href="/${this.prgAcronym}/${subSlug}">${subName}</a>
            </li>`; 
            } 
          }
          menuStr += `
          </ul>
        </li>`;
        } else {
          menuStr += `
        <li>
          <a <!--#if expr='v("subsite_section") = "${slug}"' -->data-current-section<!--#endif --> href="/${this.prgAcronym}/${slug}">${name}</a>
        </li>`;
        }
    }  
    return menuStr;
  }

  build() {  
    var headerStr =`
<!--#set var='fiscal_site_section' value='gov-agencies' -->

<!--#include virtual='/includes/header-subsite.include' -->

<header id="header-${this.prgAcronym}" class="header-subsite">
  <div class="lock">
    <a class="logotype-subsite" href="/${this.prgAcronym}">${this.prgName}</a>
    <button class="menu-button" aria-haspopup="true" aria-expanded="false" aria-controls="nav-subsite">Menu</button>

    <nav id="nav-subsite">
      <ul class="unstyled items left">`; 

  headerStr += this.defineMenu();    
  headerStr +=`       
      </ul>
    </nav>
  </div>
</header>

<main id="main">
    `;  

    return headerStr;
  }
};